package myprojects.automation.assignment2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DashboardPage {
    private WebDriver driver;
    private By logoutImage = By.cssSelector("span.employee_avatar_small");
    private By logoutButton = By.id("header_logout");
    private By menuElements = By.className("maintab");

    public DashboardPage(WebDriver driver){
        this.driver = driver;
    }

    public void clickLogoutImage(){
        driver.findElement(logoutImage).click();
    }

    public void clickLogoutButton(){
        driver.findElement(logoutButton).click();
    }

    public List<String> findMenuItems() {
        return driver.findElements(menuElements)
                .stream().map((e) -> e.getAttribute("data-submenu"))
                .collect(Collectors.toList());
    }

    public String openMenuItem(String itemId){
        By menuItem = By.cssSelector(String.format("[data-submenu=\"%s\"]", itemId));
        driver.findElement(menuItem).click();
        return driver.getTitle();
    }
}