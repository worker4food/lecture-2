package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import myprojects.automation.assignment2.pages.DashboardPage;
import myprojects.automation.assignment2.pages.LoginPage;
import org.openqa.selenium.WebDriver;

public class CheckMainMenuTest extends BaseScript{

    public static void main(String[] args) throws InterruptedException {
        WebDriver drv = getConfiguredDriver();
        LoginPage welcome = new LoginPage(drv);
        DashboardPage dash = new DashboardPage(drv);

        welcome.open();
        welcome.fillEmailInput();
        welcome.fillPasswordInput();
        welcome.clickLoginButton();

        for (String it: dash.findMenuItems()) {
            String title = dash.openMenuItem(it);
            System.out.println(title);
            drv.navigate().refresh();
            assert (title == drv.getTitle());
        }

        drv.quit();
    }
}
