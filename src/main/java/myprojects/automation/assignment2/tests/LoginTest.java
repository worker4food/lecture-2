package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import myprojects.automation.assignment2.pages.DashboardPage;
import myprojects.automation.assignment2.pages.LoginPage;
import org.openqa.selenium.WebDriver;

public class LoginTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getConfiguredDriver();
        LoginPage welcome = new LoginPage(driver);
        DashboardPage dashboard = new DashboardPage(driver);

        welcome.open();
        welcome.fillEmailInput();
        welcome.fillPasswordInput();
        welcome.clickLoginButton();

        dashboard.clickLogoutImage();
        dashboard.clickLogoutButton();

        driver.quit();
    }
}
